<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BuddyBoss_Theme
 */

get_header();
?>

<div id="primary" class="content-area bb-grid-cell">
<h1><?php the_title(); ?></h1>


  <section id="modal">
  <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Open on time first login
  </button>

  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-body">
        <div id="demo" class="carousel slide" data-ride="carousel">

        <ul class="carousel-indicators">
          <li data-target="#demo" data-slide-to="0" class="active"></li>
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <h1>test</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quo distinctio qui quas incidunt hic. Blanditiis sequi quibusdam nam dolore ratione, fuga ipsum eligendi expedita hic consequuntur, dicta odio reprehenderit!</p>
          </div>
          <div class="carousel-item">
            <h1>test 2</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quo distinctio qui quas incidunt hic. Blanditiis sequi quibusdam nam dolore ratione, fuga ipsum eligendi expedita hic consequuntur, dicta odio reprehenderit!</p>        
          </div>
          <div class="carousel-item">
            <h1>test 3</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quo distinctio qui quas incidunt hic. Blanditiis sequi quibusdam nam dolore ratione, fuga ipsum eligendi expedita hic consequuntur, dicta odio reprehenderit!</p>        
          </div>
        </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  </section>

  <main id="main" class="site-main">

    <?php
      wp_nav_menu( array(
        'theme_location' => 'dashboard',
        'menu_id'		 => 'Dashboard',
        'container'		 => false,
        'fallback_cb'	 => '',
        'menu_class'	 => 'primary-menu bb-primary-overflow', )
      );
    ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer(); ?>





