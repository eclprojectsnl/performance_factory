var $ = jQuery;


$( document ).ajaxComplete(function( event,request, settings ) {
  $(".bp-user-link strong").filter(function() {
    return $(this).text() === "U";
  }).closest('.bp-single-message-wrap').parent().addClass('send-by-you');
});

$( document ).ready(function() {

$('<div class="progress-container"><div class="progress-bar" id="myBar"></div></div>').prependTo('body');
// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.height = scrolled + "%";
}



});

jQuery(window).load(function() {
 
  /*
      Stop carousel
  */
  $('.carousel').carousel('pause');

});
